[<< back](README.md)

# Shader

* [White flash, hide flash](flash.md)
* [Video | Pixel Art Shader In Godot](https://www.youtube.com/watch?v=evdzg3c-Zho): In response to Unity retroactively changing their licensing policy, I'm retroactively porting my Pixelart Effect into Godot, but with extra features.
