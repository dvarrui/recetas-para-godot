
# Matemáticas

* [Math for Game Programmers: Building a Better Jump](https://www.youtube.com/watch?v=hG9SzQxaCm8) Math for Game Programmers: Building a Better Jump. In this 2016 GDC talk, Minor Key Games' Kyle Pittman shows how to construct natural-feeling jump trajectories from designer-friendly input like desired height and distance, mode
* [Essential Mathematics For Aspiring Game Developers](https://www.youtube.com/watch?v=DPfxjQ6sqrc)
