
# Chiptune

Herramientas GNU/Linux con licencia libre para trabajar con música y sonido "chiptune".
* [Ardour](https://ardour.org/): Estación de trabajo de audio digital multicanal. Permite trabajar con DAWS
* lmms: Para trabajar con DAWS
* [BeepBox2](https://www.beepbox.co/2_3/#6n31s7kbl00e03t4m1a7g0fj7i0r1o3210T0w1f1d1c0h6v0T0w1f1d1c0h0v0T0w1f1d1c0h0v0T2w1d1v0b4i4h4h4h4h4h8h4h4h4h4h4h4h4h4h4h4h4h4h4h4h4p217FDNhgCnGooSRJriq7gL5Qg5oW-cUHq0FDNhgpM5cuwN0eMcEhB0INN5MV02C9wo0DsjG8w0)
* [Furnace (chiptune tracker)](https://github.com/tildearrow/furnace): a multi-system chiptune tracker compatible with DefleMask modules

# Sonidos

* [rFXGen](https://github.com/raysan5/rfxgen): A simple and easy-to-use fx sounds generator, based on the amazing Dr.Petter's sfxr.
