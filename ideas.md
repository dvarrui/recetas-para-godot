
# Inspiración

* [Desert Dungeon Open Beta Update](https://www.onebitadventure.com/desert-dungeon-open-beta-update/)
* [ZX Spectrum - The Last Vampire](https://twitter.com/ZX48kSpectrum/status/1725941239894188218?t=p9tCul6Z9wnvqNTG4i1v6w&s=35)
* [Half-Life roguelike concept](https://twitter.com/MrmoTarius/status/1682826722067075075?t=v1Wjxq5uNDviiJ74eqY9HQ&s=35)

* [Twitter Bastichb64k - A little video I put together for Wizard of Wor on the C64 , a great conversion from 1983.](https://x.com/bastichb64k/status/1717980291078524991?t=jJ6ZB5GG_pTjak8Fis5Hyg&s=35)
Wizard of Wor - Commodore 1983 - Estas mecánicas pueden adaptarse a Mando buscando a Grogu en los laberintos donde se esconden los imperiales.
https://www.youtube.com/watch?v=nwEeEJQ1_Ic

## Commando

* [Commando](https://twitter.com/CommodoreBlog/status/1577443710950080512?t=_be0YfLXcd1Ht8Cpt_UFcA&s=35)
* [Commando (Arcade) - Complete Game](https://www.youtube.com/watch?v=1qctKI_t5eY)
* [Ikari warriors (Arcade) - Complete game walkthrough](https://www.youtube.com/watch?v=yQYoiZcqURg)
* [Commando twitter](https://twitter.com/CommodoreBlog/status/1653459864490786824?t=-4x9ZnpI2TgkKdSHp41c2A&s=35)
* [Who dares win II](https://twitter.com/Kenziner99/status/1717302476532052202?t=NLw-2Z_E46SPNZnJTNTHyQ&s=35)

## Maninc Miner

* [Manic Miner 1 map](https://maps.speccy.cz/map.php?id=ManicMiner1)

## Gauntlet

* [Space-Gauntlet](https://codeberg.org/Trevizer/Space-Gauntlet).Código fuente del juego Space Gauntlet creado por Trevizer en Godot.

## 1bit

* [White scape](https://nulltale.itch.io/white-scape)
* [Lambda Rogue Tileset ](https://twitter.com/MrmoTarius/status/1725259145262325943?t=n_5eJ19nqdlqGZlQaNsm-Q&s=35)

## Beat'em up

* https://x.com/dantemendes/status/1712058740013224368?t=ibSjPMj6OyqBDeo8KGj6pw&s=35

## Assets

* [Small dudes](https://twitter.com/GrimmNail/status/1646558648489869341?t=g_modw_ZPoZ4vgygSylmBg&s=35)
* [Weapon sprites](https://twitter.com/_V3X3D/status/1724420603522793673?t=5w6x-UCMeFIW0lsxDKc9-Q&s=35)
* [Choose your doodle spirit pet!](https://x.com/dom2d/status/1714877140619940122?t=aOBOgmTnj-zywsw0AbcJ4w&s=35)
* [AT-AT](https://x.com/Logiker464/status/1705227379575054755?t=VPxx4coF5HYwUfTr3i1F-g&s=35
* [X formerly Twitter](https://x.com/Logiker464/status/1705227379575054755?t=VPxx4coF5HYwUfTr3i1F-g&s=35) Flashback of the 2nd ATASCII Compo 2022. Outstanding ASCII art. Want another round?
