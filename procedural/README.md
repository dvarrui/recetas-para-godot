
# Procedural

* [Walker level generator by uheartbeast](walker-level-gen.md)
* [Laberintos 2d by KidsCancode](mazes.md)

---

* [Procedural Content Generation in Godot 3.0  Part 3: Tile-based Infinite Worlds](https://youtu.be/lc34v8pjTXk?si=2l0iub8tTirYOAJC)
    * [Text tutorial](https://kidscancode.org/blog/2018/09/godot3_procgen3/)
* [Procedural Generation in Godot 3.0 Part 4: 2D Terrain (Midpoint Displacement)](https://youtu.be/xSX1pN_dQQA?si=uJCGem0k1X7b7KrZ)
* [Procedural Generation in Godot 3.0 Part 5: Infinite 2D Terrain](https://youtu.be/QLZa1mjW-YU?si=U22nRfq8l6MI44bL)

## gdquest

* [Random Level Generation with a Walker - Godot Tutorial 1](https://youtu.be/2nk6bJBTtlA?si=PODKKqsA-gTe5uPR)
    * [Github code](https://github.com/uheartbeast/walker-level-gen)
* [Rooms in a Walker Level Generator - Godot Tutorial 2](https://youtu.be/1O83wdW5hAs?si=sSb45erzop_FoLZD)

## Others

* [PROCEDURAL Terrain Generation (with Unloading) in Godot!](https://youtu.be/cqyD2EEVD3g?si=Q-ykzdjzSCArOaqN): Easy and step by step tutorial on how to make procedural natural terrain generation in Godot 4+ (I used version 4.1 here). I also explain what each element does for beginners so you can customize


3D asset lib - No es procedural
* [Godot Road Generator Add-On](https://t.co/TPQmgMxTw4?s=35): The Godot Road Generator is a free and open source add-on for the Godot game engine for quickly generating road networks.
