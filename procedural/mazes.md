[<< back](README.md)

# Mazes


**KidsCanCode - procedural content generation**

* [Procedural Content Generation in Godot 3.0 Part 1: Mazes](https://youtu.be/YShYWaGF3Nc?si=J8AKHpN6bccD_Iyr) In this first part, we look at maze generation.
    * [Text tutorial](https://kidscancode.org/blog/2018/08/godot3_procgen1/)
    * [Code on Github](https://github.com/kidscancode/godot3_procgen_demos)
* [Procedural Content Generation in Godot 3.0 Part 2: Using Mazes](https://youtu.be/qkiyzjTqm9o?si=SqOWs6aT9IpSROH2)
    * [Text tutorial](https://kidscancode.org/blog/2018/09/godot3_procgen2/)
