```
EN CONSTRUCCIÓN!!!
```

# Recetas para GodotEngine

Artículos de otros autores, traducidos al español:
* [2d](2d/README.md)
* [Generación procedural](procedural/README.md)

## Entrevistas

* [NeoRetro: Todo el mundo puede hacer videojuegos - Locomalito](https://blog.uptodown.com/neoretro-crear-videojuegos-locomalito/)

## Tutoriales, blogs, etc.

* [GitHub - godotengine/awesome-godot](https://github.com/godotengine/awesome-godot): A curated list of free/libre plugins, scripts and add-ons for Godot
* [Godot land](https://godot.land/)
* [Kidscancode recipes](https://kidscancode.org/godot_recipes/4.x/)
* [Snopek Games](https://www.snopekgames.com/games)
