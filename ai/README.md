
# Inteligencia Artificial

* Libro sobre AI en videojuegos: http://gameaibook.org/
* [How to build a platformer AI](https://devlog.levi.dev/2021/09/building-platformer-ai-from-low-level.html): From low-level trajectories to high-level behaviors. This series of posts describes the design of the platformer AI in the Surfacer framework for God
